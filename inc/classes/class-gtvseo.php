<?php

/**
 * Electro Class
 *
 * @author   Transvelo
 * @package  electro
 */

if (!defined('ABSPATH')) {
    exit;
}

if (!class_exists('Gtvseo')) :

    /**
     * The main Electro class
     */
    class Gtvseo
    {

        private static $structured_data;

        /**
         * Setup Class
         */
        public function __construct()
        {
            // --------------
            // Filter Section
            // --------------
            add_filter('wpseo_locale', array($this, 'yst_wpseo_change_og_locale'));

            // change og:url output
            add_filter('wpseo_opengraph_url', array($this, 'my_opengraph_url'));

            // display custom post type
            add_filter('pre_get_posts', array($this, 'lay_custom_post_type'));

            // display custom post type
            add_filter('pre_get_posts', array($this, 'get_custom_post_type'));

            // disable yoast schema
            add_filter('wpseo_json_ld_output', '__return_false');

            // Remove "Category:" from archive title
            add_filter('get_the_archive_title', function ($title) {
                return preg_replace('/^\w+: /', '', $title);
            });

            add_filter('tiny_mce_plugins', array($this, 'disable_emojis_tinymce'));

            // --------------
            // Action Section
            // --------------
            add_action('wp_enqueue_scripts', array($this, 'gtv_files'));

            /* Disable emoji */
            add_action('init', array($this, 'disable_emojis'));

            /* Kích hoạt hàm tạo custom post type */
            // add_action('init', array($this, 'ebook_post_type'));

            // add canonical tag to url
            add_action('wp_head', 'rel_canonical');
        }

        function yst_wpseo_change_og_locale($locale)
        {
            return 'vi_VN';
        }

        function my_opengraph_url($url)
        {
            return str_replace('https://gtvseo.com/', 'https://gtvseo.com', $url);
        }

        function lay_custom_post_type($query)
        {
            if (is_home() && $query->is_main_query())
                $query->set('post_type', array('post', 'video'));
            return $query;
        }

        function get_custom_post_type($query)
        {
            if (is_home() && $query->is_main_query())
                $query->set('post_type', array('post', 'ebook'));
            return $query;
        }

        function disable_emojis()
        {
            remove_action('wp_head', 'print_emoji_detection_script', 7);
            remove_action('admin_print_scripts', 'print_emoji_detection_script');
            remove_action('wp_print_styles', 'print_emoji_styles');
            remove_action('admin_print_styles', 'print_emoji_styles');
            remove_filter('the_content_feed', 'wp_staticize_emoji');
            remove_filter('comment_text_rss', 'wp_staticize_emoji');
            remove_filter('wp_mail', 'wp_staticize_emoji_for_email');
        }

        function disable_emojis_tinymce($plugins)
        {
            if (is_array($plugins)) {
                return array_diff($plugins, array('wpemoji'));
            } else {
                return array();
            }
        }

        function gtv_files()
        {
            wp_enqueue_script('main-gtv-js', get_theme_file_uri('/assets/js/scripts-bundled.js'), NULL, NULL, true);
            wp_enqueue_style('owl-carousel-styles', get_theme_file_uri('/assets/vendors/owl.carousel.min.css'));
            wp_enqueue_style('fontawsome', '//use.fontawesome.com/releases/v5.5.0/css/all.css');
            // wp_enqueue_script('font-awesome', get_theme_file_uri('/js/font-awesome.js'), NULL, NULL, true);
            // wp_enqueue_script('video-js', get_theme_file_uri('/js/modules/video.js'), NULL, NULL, true);

            // wp_enqueue_style('custom-google-fonts', '//fonts.googleapis.com/css?family=Roboto+Condensed:300,300i,400,400i,700,700i|Roboto:100,300,400,400i,700,700i');
            wp_enqueue_style('add-google-fonts', '//fonts.googleapis.com/css2?family=Be+Vietnam:wght@400;500;600;700;800&display=swap');


            $strLink =  strval(get_the_permalink());

            if (is_front_page() || is_home()) {
                wp_deregister_script('jquery');
                wp_enqueue_style('home-gtv-styles', get_theme_file_uri('assets/css/home.css'));
                wp_enqueue_script('home-js', get_theme_file_uri('assets/js/vendors/home.js'), NULL, NULL, true);
            } else {
                wp_enqueue_style('main-gtv-styles', get_stylesheet_uri());
            }


            if(is_single()){
                wp_enqueue_script('rAF', get_theme_file_uri('assets/js/modules/rAF.js'), NULL, NULL, true);
                wp_enqueue_script('ResizeSensor', get_theme_file_uri('assets/js/modules/ResizeSensor.js'), NULL, NULL, true);
                wp_enqueue_script('sticky-sidebar', get_theme_file_uri('assets/js/modules/sticky-sidebar.js'), NULL, NULL, true);
                wp_enqueue_script('owl-carousel', get_theme_file_uri('assets/js/modules/owl.carousel.js'), NULL, NULL, true);
                wp_enqueue_script('blog-sidebar', get_theme_file_uri('assets/js/vendors/blogSideBar.js'), NULL, NULL, true);
                wp_enqueue_style('grids-responsive', '//unpkg.com/purecss@1.0.1/build/grids-responsive-min.css');

            }
            if(is_404()){
                wp_enqueue_style('404-styles', get_theme_file_uri('assets/css/404.css'));
            }



        }


        function wpse240134_wp_admin_bar_init()
        {
            _wp_admin_bar_init();
        }

        function mg_news_pagination_rewrite()
        {
            add_rewrite_rule(get_option('category_base') . '/page/?([0-9]{1,})/?$', 'index.php?pagename=' . get_option('category_base') . '&paged=$matches[1]', 'top');
        }
    }
endif;

return new Gtvseo();
