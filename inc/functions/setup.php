<?php

/**
 * gtvseo Setup Functions
 *
 * Setup sidebar
 *
 * @author 		Vu
 * @package 	gtvseo/Setup
 * @version     1.0.0
 */

if (!function_exists('gtvseo_setup_sidebars')) {
    /**
     * Setup Sidebars available in gtvseo
     */
    function gtvseo_setup_sidebars()
    {

        register_sidebar(array(
            'name' => 'Footer Sidebar 1',
            'id' => 'footer-sidebar-1',
            'description' => 'Appears in the footer area',
            'before_widget' => '<aside id="%1$s" class="footer-widget %2$s">',
            'after_widget' => '</aside>',
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>',
        ));
        register_sidebar(array(
            'name' => 'Blog Sidebar',
            'id' => 'blog-sidebar',
            'description' => 'Appears in the blog left area',
            'before_widget' => '<aside id="%1$s" class="footer-widget %2$s">',
            'after_widget' => '</aside>',
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>',
        ));
        register_sidebar(array(
            'name' => 'Blog TOC',
            'id' => 'blog-toc',
            'description' => 'Appears in the blog top area',
            'before_widget' => '<aside id="%1$s" class="toc-widget %2$s">',
            'after_widget' => '</aside>',
            'before_title' => '',
            'after_title' => '',
        ));
    }

    add_filter('widgets_init', 'gtvseo_setup_sidebars');
}

// be nice to the children (make it pluggable)
if (!function_exists('butter_modified_fields')) :

    function butter_modified_fields($contact_methods)
    {
        $contact_methods['skype'] = __('Skype profile Url', 'butter');
        $contact_methods['twitter'] = __('Twitter profile Url', 'butter');
        $contact_methods['youtube'] = __('Youtube profile Url', 'butter');
        $contact_methods['instagram'] = __('Instagram profile Url', 'butter');
        $contact_methods['facebook'] = __('Facebook profile Url', 'butter');
        $contact_methods['linkedin'] = __('LinkedIn profile Url', 'butter');
        $contact_methods['pinterest'] = __('Pinterest profile URL', 'butter');

        return $contact_methods;
    }

    add_filter('user_contactmethods', 'butter_modified_fields');

endif;

add_action('admin_init', 'my_general_section');
function my_general_section() {
    add_settings_section(
        'my_banner_blog_section', // Section ID
        'Setting Blogs Banner', // Section Title
        'my_section_options_callback', // Callback
        'general' // What Page?  This makes the section show up on the General Settings Page
    );

    add_settings_field( // Option 1
        'link_banner', // Option ID
        'Banner Link', // Label
        'my_textbox_callback', // !important - This is where the args go!
        'general', // Page it will be displayed (General Settings)
        'my_banner_blog_section', // Name of our section
        array( // The $args
            'link_banner' // Should match Option ID
        )
    );

    add_settings_field( // Option 2
        'img_url_banner', // Option ID
        'Image Url', // Label
        'my_textbox_callback', // !important - This is where the args go!
        'general', // Page it will be displayed
        'my_banner_blog_section', // Name of our section (General Settings)
        array( // The $args
            'img_url_banner' // Should match Option ID
        )
    );

    add_settings_field( // Option 3
        'img_alt_banner', // Option ID
        'Image Alt', // Label
        'my_textbox_callback', // !important - This is where the args go!
        'general', // Page it will be displayed
        'my_banner_blog_section', // Name of our section (General Settings)
        array( // The $args
            'img_alt_banner' // Should match Option ID
        )
    );

    register_setting('general','link_banner', 'esc_attr');
    register_setting('general','img_url_banner', 'esc_attr');
    register_setting('general','img_alt_banner', 'esc_attr');
}

function my_section_options_callback() { // Section Callback
    echo '<p>Setting Blog banner</p>';
}

function my_textbox_callback($args) {  // Textbox Callback
    $option = get_option($args[0]);
    echo '<input type="text" id="'. $args[0] .'" name="'. $args[0] .'" value="' . $option . '" />';
}

// Add field to User Profile
function fb_add_custom_user_profile_fields( $user ) {
    ?>
    <h3><?php _e('Extra Profile Information', 'gtvseo.com'); ?></h3>

    <table class="form-table">
        <tr>
            <th>
                <label for="image_link"><?php _e('User image', 'gtvseo.com'); ?>
            </label></th>
            <td>
                <input type="text" name="image_link" id="image_link" value="<?php echo esc_attr( get_the_author_meta( 'image_link', $user->ID ) ); ?>" class="regular-text" /><br />
                <span class="description"><?php _e('Please enter User image link.', 'gtvseo.com'); ?></span>
            </td>
        </tr>
    </table>
<?php }

function fb_save_custom_user_profile_fields( $user_id ) {

    if ( !current_user_can( 'edit_user', $user_id ) )
        return FALSE;

    update_usermeta( $user_id, 'image_link', $_POST['image_link'] );
}

add_action( 'show_user_profile', 'fb_add_custom_user_profile_fields' );
add_action( 'edit_user_profile', 'fb_add_custom_user_profile_fields' );

add_action( 'personal_options_update', 'fb_save_custom_user_profile_fields' );
add_action( 'edit_user_profile_update', 'fb_save_custom_user_profile_fields' );
