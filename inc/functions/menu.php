<?php

/**
 * gtvseo Menu Functions
 *
 * Menu related functions and menu locations
 *
 * @author 		Vu
 * @package 	gtvseo/Functions
 * @version     1.0.0
 */

if (!defined('ABSPATH')) {
    exit;
}

function gtvseo_menu()
{
    register_nav_menu('headerMenuLocation', 'Header Menu Location');
    register_nav_menu('blogsubmenu', 'Blog detail Category menu');
    register_nav_menu('footerLocationOne', 'Footer Location One');
    register_nav_menu('footerLocationTwo', 'Footer Location Two');
    add_theme_support('title-tag');
    add_theme_support('post-thumbnails');

    // add_image_size('professorLandscape', 400, 260, false); // 3rd argument: crop
    // add_image_size('professorPortrait', 480, 650, false);
}

add_action('after_setup_theme', 'gtvseo_menu');
