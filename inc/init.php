<?php 

/**
 * Classes
 * Load classes that are used by various functions
 */
require get_template_directory() . '/inc/classes/class-gtvseo.php';

/**
 * Setup.
 * Enqueue styles, register widget regions, etc.
 */
require get_template_directory() . '/inc/functions/setup.php';
require get_template_directory() . '/inc/functions/menu.php';