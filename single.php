<?php get_header(); ?>

<!-- category-menu -->
<?php get_template_part('partials/category-menu'); ?>
<!-- /category-menu -->

<?php while (have_posts()) {
      the_post();  ?>

<div id="header-banner">
</div>

<!-- Blog Content -->
<section class="blog">


  <?php if (has_post_thumbnail()) : ?>

    <div class="featured-img">

      <?php $image = wp_get_attachment_image_src(get_post_thumbnail_id(), 'single-post-thumbnail'); ?>

      <img src='<?php echo $image[0]; ?>' alt="<?php echo $image[0] ?>" />

    </div>
  <?php endif; ?>

  <!-- Breadcrumb -->
  <?php
    get_template_part('templates/blog-detail/breadcrumb');
  ?>
  <!-- End of Breadcrumb -->

  <div class="blog-box">

      <?php
        get_template_part('templates/blog-detail/post-meta');
      ?>

      <div class="pure-g container">

        <div class="pure-u-1 pure-u-lg-5-6" id="content" itemscope itemtype="http://schema.org/BlogPosting" itemid="#article">

          <div class="w-blog-list" >

            <?php
            get_template_part('templates/blog-detail/single-content');

            get_template_part('templates/blog-detail/author-box')
            ?>
          </div>
        </div>
        <div class="pure-u-1 pure-u-lg-1-6">
          <div class="banner-qc" id="QC">

            <?php if (get_field("url_banner") && get_field("image_banner")) : ?>
              <a href="<?php echo get_field("url_banner") ?>" target="_blank">
                <img src="<?php echo get_field("image_banner") ?>"
                alt="
                  <?php
                    if(get_field("alt_banner")){
                      echo get_field("alt_banner");
                    }else{ ?>
                      khóa seo fundamental gtvseo
                    <?php }
                  ?>
                ">
              </a>
            <?php elseif (get_option('link_banner') && get_option('img_url_banner')) : ?>
              <a href="<?php echo get_option('link_banner') ?>" target="_blank">
                <img src="<?php echo get_option('img_url_banner') ?>"
                alt="
                  <?php
                    if(get_option('img_alt_banner')){
                      echo get_option('img_alt_banner');
                    }else{ ?>
                      khóa seo fundamental gtvseo
                    <?php }
                  ?>
                ">
              </a>
            <?php else : ?>
              <a href="https://gtvseo.com/seo-fundamental-trial/?utm_source=BlogWebsite&utm_medium=banner&utm_campaign=seofundamental" target="_blank">
                <img src="https://gtvseo.com/images/banner-qc-tang-5000-traffic.jpg" alt="khóa seo fundamental gtvseo">
              </a>
            <?php endif; ?>

          </div>
        </div>
      </div>
  </div>

</section>
<!-- /Blog Content -->


<?php
  get_template_part('templates/blog-detail/related-posts')
?>

<?php } ?>

<?php get_footer(); ?>