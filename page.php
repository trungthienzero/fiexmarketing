<?php get_header(); ?>

<!-- category-menu -->
<?php get_template_part('partials/category-menu'); ?>
<!-- /category-menu -->


<!-- Breadcrumb -->
<section class="breadcrumb">
  <div class="breadcrumb-box">
    <?php
    if (function_exists('yoast_breadcrumb')) {
      yoast_breadcrumb('<ul>', '</ul>');
    }
    ?>
  </div>
</section>
<!-- End of Breadcrumb -->


<!-- Blog Content -->
<section class="blog">
  <div class="blog-box">
    <div class="pure-g container">
      <div class="pure-u-1 pure-u-md-1-4" id="sidebar">
        <div class="sidebar_inner" id="sidebar_inner">
          <p class="title"><strong>Nội dung bài viết</strong></p>
          <ul class="table-of-contents">
          </ul>
        </div>
      </div>
      <div class="pure-u-1 pure-u-sm-1 pure-u-md-7-12" id="content">

        <div class="w-blog-list">
          <?php
          while (have_posts()) {
            the_post();


            get_template_part( 'templates/blog-detail/single-content');
          }

          ?>

          <?php get_template_part('templates/blog-detail/author-box') ?>
        </div>

      </div>
      <div class="banner-qc pure-u-md-1-6" id="QC">

          <?php if (get_field("url_banner") && get_field("image_banner")) : ?>
            <a href="<?php echo get_field("url_banner") ?>" target="_blank">
              <img src="<?php echo get_field("image_banner") ?>"
              alt="
                <?php
                  if(get_field("alt_banner")){
                    echo get_field("alt_banner");
                  }else{ ?>
                    khóa seo fundamental gtvseo
                  <?php }
                ?>
              ">
            </a>
          <?php elseif (get_option('link_banner') && get_option('img_url_banner')) : ?>
            <a href="<?php echo get_option('link_banner') ?>" target="_blank">
              <img src="<?php echo get_option('img_url_banner') ?>"
              alt="
                <?php
                  if(get_option('img_alt_banner')){
                    echo get_option('img_alt_banner');
                  }else{ ?>
                    khóa seo fundamental gtvseo
                  <?php }
                ?>
              ">
            </a>
          <?php else : ?>
            <a href="https://gtvseo.com/seo-fundamental-trial/?utm_source=BlogWebsite&utm_medium=banner&utm_campaign=seofundamental" target="_blank">
              <img src="https://gtvseo.com/images/banner-qc-tang-5000-traffic.jpg" alt="khóa seo fundamental gtvseo">
            </a>
          <?php endif; ?>

        </div>
    </div>
  </div>
</section>
<!-- /Blog Content -->

<?php
get_template_part('templates/blog-detail/related-posts')
?>

<?php get_footer(); ?>