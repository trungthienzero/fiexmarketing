<?php
get_header(); ?>


<!-- secondary-menu -->
<?php get_template_part('partials/secondary-menu'); ?>
<!-- /secondary-menu -->

<!-- Banner -->
<section class="banner-tong">
    <div class="banner-title">
        <h1 class="title"><?php echo get_the_archive_title(); ?></h1>
    </div>
    <!-- <div class="banner-wrap">
        <div class="banner-content">
            <p><?php the_archive_description(); ?></p>
        </div>
    </div> -->
</section>
<!-- Banner -->

<!-- Start of Breadcrumb -->
<section class="breadcrumb category-breadcrumb" itemprop="BreadcrumbList" itemscope itemtype="https://schema.org/BreadcrumbList" itemid="#breadcrumb">
	<div class="breadcrumb-box">
		<ul>
			<span itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
				<meta itemprop="position" content="1">
					<a itemprop="item" href="https://gtvseo.com/">
						<span itemprop="name">Trang chủ</span>
					</a> »
			</span>
            <?php
                if(is_post_type_archive('post')){?>
                    <span itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                        <meta itemprop="position" content="2">
                            <a itemprop="item" href="https://gtvseo.com/blog/">
                                <span itemprop="name">Blog</span>
                            </a> »
                    </span>
                <?php }
            ?>
			<span itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
				<meta itemprop="position" content="3">
				<span itemprop="item" content="<?php global $wp; echo home_url( $wp->request ) ?>/">
					<span class="breadcrumb_last" aria-current="page" itemprop="name"><?php the_archive_title() ?></span>
				</span>
			</span>
		</ul>
	</div>
</section>
<!-- End of Breadcrumb -->

  <!--  -->
  <section class="post-box">
      <div class="post-list">
          <ul class="clearfix">
              <?php
              while (have_posts()) {
                  the_post();

                  if( $wp_query->current_post <1) :?>
                      <li class="highlight-post-box">
                          <div class="highlight-img">
                              <?php if ( has_post_thumbnail()) :?>
                                  <?php the_post_thumbnail(); ?>
                              <?php else: ?>
                                  <img src="<?php echo get_template_directory_uri(); ?>/assets/images/logo.png" alt="">
                              <?php endif; ?>
                          </div>
                          <div class="highlight-post-content">
                              <div class="content-box">
                              <a href="<?php the_permalink(); ?>" class="post-title"><h3 class="title"><?php the_title(); ?></h3></a>
                              </div>
                          </div>
                          <div class="link">
                              <i class="fas fa-wifi"></i><a href="<?php the_permalink(); ?>"><strong>Đọc bài viết</strong><i class="fas fa-chevron-right"></i></a>
                          </div>
                      </li>
                  <?php endif; ?>
              <?php } ?>

                  <li class="highlight-post-box form">
                      <div class="img-wrap">
                          <img src="<?php echo get_template_directory_uri(); ?>/assets/images/giphy.gif" alt="">
                      </div>
                      <div class="form-content-box">
                        <div class="form-content">
                            <p class="title">Checklist SEO Audit</p>
                            <a href="https://gtvseo.com/checklist-seo-audit/" target="_blank"><strong>Download ngay!</strong></a>
                        </div>
                      </div>
                  </li>

              <?php  while (have_posts()) {
                  the_post();
                  if( $wp_query->current_post>=1 && $wp_query->current_post <8) :?>
                      <li class="highlight-post-box">
                          <div class="highlight-img">
                              <?php if ( has_post_thumbnail()) :?>
                                  <?php the_post_thumbnail(); ?>
                              <?php else: ?>
                                  <img src="<?php echo get_template_directory_uri(); ?>/assets/images/logo.png" alt="logo gtvseo">
                              <?php endif; ?>
                          </div>
                          <div class="highlight-post-content">
                              <div class="content-box">
                              <a href="<?php the_permalink(); ?>" class="post-title"><h3 class="title"><?php the_title(); ?></h3></a>
                              </div>
                          </div>
                          <div class="link">
                              <i class="fas fa-wifi"></i><a href="<?php the_permalink(); ?>"><strong>Đọc bài viết</strong><i class="fas fa-chevron-right"></i></a>
                          </div>
                      </li>
                  <?php endif; ?>
              <?php } ?>

              <?php  while (have_posts()) {
                  the_post();
                  if( $wp_query->current_post>7 && $wp_query->current_post<9) :?>
                      <li class="highlight-post-box cta">
                          <div class="highlight-img">
                              <?php if ( has_post_thumbnail()) :?>
                                  <?php the_post_thumbnail(); ?>
                              <?php else: ?>
                                  <img src="<?php echo get_template_directory_uri(); ?>/assets/images/logo.png" alt="">
                              <?php endif; ?>
                          </div>
                          <div class="highlight-post-content">
                              <a href="<?php the_permalink(); ?>" class="post-title"><h3 class="title"><?php the_title(); ?></h3></a>
                              <p class="description"><?php echo the_excerpt(); ?></p>
                              <a href="<?php the_permalink(); ?>" class="link"><strong>Đọc tiếp</strong></a>
                          </div>
                      </li>
                  <?php endif; ?>
              <?php } ?>

              <?php  while (have_posts()) {
                  the_post();
                  if( $wp_query->current_post>8 && $wp_query->current_post<10) :?>
                      <li class="highlight-post-box">
                          <div class="highlight-img">
                              <?php if ( has_post_thumbnail()) :?>
                                  <?php the_post_thumbnail(); ?>
                              <?php else: ?>
                                  <img src="<?php echo get_template_directory_uri(); ?>/assets/images/logo.png" alt="">
                              <?php endif; ?>
                          </div>
                          <div class="highlight-post-content">
                              <div class="content-box">
                              <a href="<?php the_permalink(); ?>" class="post-title"><h3 class="title"><?php the_title(); ?></h3></a>
                              </div>
                          </div>
                          <div class="link">
                              <i class="fas fa-wifi"></i><a href="<?php the_permalink(); ?>"><strong>Đọc bài viết</strong><i class="fas fa-chevron-right"></i></a>
                          </div>
                      </li>
                  <?php endif; ?>
              <?php } ?>

                  <li class="highlight-post-box qc">
                      <div class="img-wrap">
                          <img src="https://content.cdntwrk.com/files/aHViPTIxOCZjbWQ9Y3RhX2JhY2tncm91bmQmY3RhX2lkPTI1NzE0MiZtb2RpZmllZD0yMDE5LTA5LTA0IDE0OjQzOjIyJnNpZz1kNWNhOGQ1OTczOTIzODZiMjk3NmFjZWQ1MTE0NjI0NQ%253D%253D" alt="">
                      </div>
                      <div class="qc-content-box">
                        <div class="qc-content">
                            <p class="title">Tài liệu SEO trang sức khoẻ</p>
                            <a href="https://gtvseo.com/tai-lieu-seo-trang-suc-khoe/" target="_blank"><strong>Download ngay!</strong></a>
                        </div>
                      </div>
                  </li>

              <?php while (have_posts()) {
                  the_post();
                  if($wp_query->current_post >=10) :?>
                      <li class="highlight-post-box">
                      <div class="highlight-img">
                          <?php if ( has_post_thumbnail()) :?>
                              <?php the_post_thumbnail(); ?>
                          <?php else: ?>
                              <img src="<?php echo get_template_directory_uri(); ?>/assets/images/logo.png" alt="logo gtvseo">
                          <?php endif; ?>
                      </div>
                      <div class="highlight-post-content">
                          <div class="content-box">
                          <a href="<?php the_permalink(); ?>" class="post-title"><h3 class="title"><?php the_title(); ?></h3></a>
                          </div>
                      </div>
                      <div class="link">
                          <i class="fas fa-wifi"></i><a href="<?php the_permalink(); ?>"><strong>Đọc bài viết</strong><i class="fas fa-chevron-right"></i></a>
                      </div>
                  </li>
                  <?php endif; ?>
              <?php }  wp_reset_postdata(); ?>

                  <li class="highlight-post-box qc">
                      <div class="img-wrap">
                          <img src="https://content.cdntwrk.com/files/aHViPTIxOCZjbWQ9Y3RhX2JhY2tncm91bmQmY3RhX2lkPTExNTQxOSZtb2RpZmllZD0yMDE4LTAzLTA3IDIwOjUyOjQzJnNpZz1mOTBkMTUxYTQ2MTgxNWM2Y2YyYzNlZmE0M2MzMDE1ZA%253D%253D" alt="">
                      </div>
                      <div class="qc-content-box">
                        <div class="qc-content">
                            <p class="title">Tài liệu Expand List Post</p>
                            <a href="https://gtvseo.com/expanded-list-post/" target="_blank"><strong>Download ngay!</strong></a>
                        </div>
                      </div>
                  </li>
          </ul>
          <div class="pagination right-align">
              <?php
                  echo paginate_links();
              ?>
          </div>
      </div>
  </section>
  <!--  -->



<?php get_footer();

?>