var banner = new StickySidebar('#QC',{
    topSpacing: 100,
    bottomSpacing: 20
});


function toTop(){
  jQuery("html, body").animate({scrollTop : 0},700);
};

// When the user scrolls the page, execute myFunction
window.onscroll = function() {myFunction()};

function myFunction() {
    var winScroll = document.body.scrollTop || document.documentElement.scrollTop;
    var height = document.documentElement.scrollHeight - document.documentElement.clientHeight;
    var scrolled = (winScroll / height) * 100;
    document.getElementById("myBar").style.width = scrolled + "%";

    if (document.body.scrollTop > 300 || document.documentElement.scrollTop > 300) {
        document.getElementById("social-share").className = "social-share sticky";
      } else {
        document.getElementById("social-share").className = "social-share";
      }
}

