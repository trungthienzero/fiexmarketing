// function schedule running at home page
function schedule() {
    for (a = 1; a <= 10; a++) {
        if (document.getElementById(`schedule-step-${a}`).checked == true) {

            for (i = 0; i < 10; i++) {
                var test = document.getElementsByClassName("schedule-content")[i];
                test.style.display = "none";

            }
            document.getElementById(`schedule-content-step-${a}`).style.display = "block";
        }
    }
}

// show detail youtube video for video page
function showDetails(embed) {
    var embedCode = embed.getAttribute("data-embed");
    // document.getElementById('modal-box').innerHTML = embedUrl;
    var src = `https://www.youtube.com/embed/` + embedCode;
    document.getElementById("myFrame").src = src;
    document.getElementById('modal').style.display = "block";
}

function closeModal() {
    document.getElementById("myFrame").src = "";
    document.getElementById('modal').style.display = "none";
}

window.schedule = schedule;
window.showDetails = showDetails;
window.closeModal = closeModal;
