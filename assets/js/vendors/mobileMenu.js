var category = $('#category-menu');

var main_menu = $('#site-header');

$(window).scroll(function () {
  if ($(window).scrollTop() > 94) {
    category.addClass('sticky-category');
    main_menu.addClass('menu-scroll');
  } else {
    category.removeClass('sticky-category');
    main_menu.removeClass('menu-scroll');
  }
});

var sub_menu = $('#sub-menu');

$(window).scroll(function () {
  if ($(window).scrollTop() > 94) {
      sub_menu.addClass('onscroll');
  } else {
      sub_menu.removeClass('onscroll');
  }
});

var logo_scroll = $('#site-logo-inner');

var menu = $('#site-header');

$(window).scroll(function () {
    if ($(window).scrollTop() > 200) {
        logo_scroll.addClass('logo-scroll');
        menu.addClass('menu-scroll');
    } else {
        logo_scroll.removeClass('logo-scroll');
        menu.removeClass('menu-scroll');
    }
});

// Mobile menu
$('#open').click(function () {
    $('#site-navigation-wrap').toggleClass('display slideRight');

    $('#open').toggleClass("close");
});

$('#button').click(function(){
  window.scrollTo(0,0);
})

function toTop(){
  jQuery("html, body").animate({scrollTop : 0},700);
};
