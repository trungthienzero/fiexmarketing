// import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel';

$(window).on('load resize orientationchange', function () {
    // responsive nav
    var responsiveNav = $('#toggle-nav');
    var navBar = $('.nav-bar');

    responsiveNav.on('click', function (e) {
        e.preventDefault();
        console.log(navBar);
        navBar.toggleClass('active')
    });

    // pseudo active
    if ($('#docs').length) {
        var sidenav = $('ul.side-nav').find('a');
        var url = window.location.pathname.split('/');
        var url = url[url.length - 1];

        sidenav.each(function (i, e) {
            var active = $(e).attr('href');

            if (active === url) {
                $(e).parent('li').addClass('active');
                return false;
            }
        });
    }

    var testimonial = $('.testimonial-carousel');
    testimonial.owlCarousel({
        margin: 30,
        nav: true,
        loop: true,
        // center: true,
        item: 3,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 2
            },
            1000: {
                items: 3
            }
        }
    })

    var employee = $('.employee-carousel');
    employee.owlCarousel({
        center: true,
        items: 3,
        loop: true,
        nav: true,
        margin: 20,
        responsive: {
            0: {
                items: 1,
            },
            576:{
                items: 1,
            },
            768:{
                items: 1,
            },
            1024:{
                items: 3,
            },
        }
    });

    $('#blog .owl-carousel').owlCarousel({
        loop: true,
        margin: 10,
        responsiveClass: true,
        dots: false,
        nav: true,
        navText: ['&amp;amp;lt;i class="fa fa-arrow-left fa-2x"&amp;amp;gt;&amp;amp;lt;/i&amp;amp;gt;', '&amp;amp;lt;i class="fa fa-arrow-right fa-2x"&amp;amp;gt;&amp;amp;lt;/i&amp;amp;gt;'], //Note, if you are not using Font Awesome in your theme, you can change this to Previous &amp;amp;amp; Next
        responsive: {
            0: {
                items: 1,
                //nav:true
            },
            767: {
                items: 2,
                //nav:false
            },
        }
    });

    $(".text-carousel").owlCarousel({
        autoplayHoverPause: true,
        animateOut: 'fadeOutUpBig',
        animateIn: 'fadeInUpBig',
        autoplay: true,
        autoplayTimeout: 2000,
        items: 1,
        margin: 0,
        loop: true,
        stagePadding: 0,
        smartSpeed: 450,
        nav: false
    });

    var related = $('.related-carousel');
    related.owlCarousel({
        margin: 50,
        nav: true,
        loop: true,
        // center: true,
        item: 4,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 2
            },
            1000: {
                items: 4
            }
        }
    })

    var brand = $('.brand-carousel');
    brand.owlCarousel({
        margin: 50,
        nav: true,
        loop: true,
        // center: true,
        item: 5,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 3
            },
            1000: {
                items: 5
            }
        }
    })

    var advertisement = $('.advertisement-carousel');
    advertisement.owlCarousel({
        margin: 15,
        nav: true,
        loop: true,
        // center: true,
        item: 4,
        responsive: {
            0: {
                items: 2
            },
            600: {
                items: 3
            },
            1000: {
                items: 5
            }
        }
    });

});

$('#button').click(function(){
    window.scrollTo(0,0);
})

// show detail youtube video for video page
function showDetails(embed) {
    var embedCode = embed.getAttribute("data-embed");
    // document.getElementById('modal-box').innerHTML = embedUrl;
    var src = `https://www.youtube.com/embed/` + embedCode;
    document.getElementById("myFrame").src = src;
    document.getElementById('modal').style.display = "block";
}

function closeModal() {
    document.getElementById("myFrame").src = "";
    document.getElementById('modal').style.display = "none";
}

window.showDetails = showDetails;
window.closeModal = closeModal;