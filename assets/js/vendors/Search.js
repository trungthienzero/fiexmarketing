class Search{
    constructor(){
        this.searchField = jQuery("#search-term");
        this.resultsDiv = jQuery("#search-overlay-result");
        this.toggleSearch();
        this.events();
        this.searchOpen = false;
        this.typingTimer;
        this.isSpinnerVisible = false;
        this.previousValue;
    }

    toggleSearch() {
        jQuery('a[href="#search"]').on('click', function(event) {
            event.preventDefault();
            jQuery('#search').addClass('open');
            jQuery('#search > form > input[type="search"]').focus();
            this.searchOpen = true;
        });

        jQuery('#search, #search button.close').on('click keyup', function(event) {
            if (event.target == this || event.target.className == 'close' || event.keyCode == 27) {
                jQuery(this).removeClass('open');
                this.searchOpen = false;
            }
        });


        //Do not include! This prevents the form from submitting for DEMO purposes only!
        jQuery('form').submit(function(event) {
            event.preventDefault();
            return false;
        })
    };

    events(){
        jQuery(document).on("keydown", this.keyPressDispatcher.bind(this));
        this.searchField.on("keyup", this.typingLogic.bind(this));
    }

    typingLogic(){

        if(this.searchField.val() != this.previousValue){
            clearTimeout(this.typingTimer);

            if(this.searchField.val()){
                if(!this.isSpinnerVisible){
                    this.resultsDiv.html('<div class="preloader-inner"></div>');
                    this.isSpinnerVisible = true;
                }

                this.typingTimer = setTimeout(this.getResults.bind(this), 2000);
            } else {
                this.resultsDiv.html('');
                this.isSpinnerVisible = false;
            }

        }


        this.previousValue = this.searchField.val();
    }

    getResults(){
        //this.resultsDiv.html("testing");
        //this.isSpinnerVisible = false;
        jQuery.getJSON('https://gtvseo.com/wp-json/wp/v2/posts?search='+ this.searchField.val(), posts =>{
            this.resultsDiv.html(`
                <section class="post-box">
                    <div class="post-list">
                        <ul class="clearfix">
                            ${posts.map(item =>`
                                <li class="highlight-post-box">
                                    <div class="highlight-post-content">
                                        <div class="content-box">
                                        <a href="${item.link}" class="post-title"><h3 class="title">${item.title.rendered}</h3></a>
                                        </div>
                                    </div>
                                </li>
                            ` ).join('')}
                        </ul>
                    </div>
                </section>
            `);
        });
    }

    keyPressDispatcher(e){
        // console.log(e.keyCode);

        if(e.keyCode == 83 && !this.searchOpen && !jQuery("input, textarea").is(':focus')){
            jQuery('#search').addClass('open');
        }

        if(e.keyCode == 27 && this.searchOpen){
            jQuery('#search').removeClass('open');
        }

    }

}

export default Search;
