//Accordion mobile
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
    acc[i].addEventListener("click", function () {
        this.classList.toggle("active");
        var panel = this.nextElementSibling;
        if (panel.style.maxHeight) {
            panel.style.maxHeight = null;
        } else {
            panel.style.maxHeight = panel.scrollHeight + "px";
        }
    });
}

function schedule(){

    for(var a=1;a<=10;a++){
        if(document.getElementById(`schedule-step-${a}`).checked == true){

            for(var i=0;i<10;i++){
                var test = document.getElementsByClassName("schedule-content")[i];
                test.style.display = "none";

            }
            document.getElementById(`schedule-content-step-${a}`).style.display = "block";
        }
    }
}

function toTop(){
    jQuery("html, body").animate({scrollTop : 0},700);
};