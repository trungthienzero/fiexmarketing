var apply = $('#apply');

var toc = $('#toc');

var bodyHeight = document.getElementById('body').offsetHeight;

var footerHeight = document.getElementById('footer').offsetHeight;

var tocHeight = document.getElementById('toc').offsetHeight;

console.log(bodyHeight);

console.log(footerHeight);

console.log(tocHeight);


$(window).scroll(function() {
    if ($(window).scrollTop() > 450) {
        apply.addClass('scroll');
        toc.addClass('scroll');

    } else {
        apply.removeClass('scroll');
        toc.removeClass('scroll');
    }
});

var pop = document.getElementById('popup');

var close = document.getElementById('close');

function openPop(){
    pop.style.display = "block";
}

close.onclick = function(){{
    pop.style.display = "none";
}}

var body = document.getElementById('body')
console.log(body.offsetHeight);

// var footer = document.getElementById('footer')
// console.log(footer.offsetHeight);

$(document).ready(function() {
    var banner = $('.banner-carousel');
    banner.owlCarousel({
        center: true,
        items: 1,
        loop: true,
        // nav: true,
        margin: 20,
        responsiveClass: true,
        autoplay:true,
        autoplayTimeout:2000,
        autoplayHoverPause:true,
        responsive: {
            0: {
                items: 1,
            },
            1024:{
                items: 1,
            }
        }
    });
    var owl = $('.related-job');
    owl.owlCarousel({
        center: true,
        items: 2,
        loop: true,
        // nav: true,
        margin: 20,
        responsive: {
            0: {
                items: 1,
            },
            576:{
                items: 1,
            },
            768:{
                items: 1,
            },
            1024:{
                items: 1,
            },
            1367:{
                items: 1,
            }
        }
    });

    owl.on('mousewheel', '.owl-stage', function (e) {
        if (e.deltaY>0) {
            owl.trigger('next.owl');
        } else {
            owl.trigger('prev.owl');
        }
        e.preventDefault();
    });
})

function core(){

    for(i=1;i<=3;i++){

        if(document.getElementById(`value-${i}`).checked == true){
            for( j=0;j<3;j++){
                var content = document.getElementsByClassName("content-box-wrap")[j];

                content.style.display = "none";

            }
            document.getElementById(`content-wrap-${i}`).style.display = "block";

        }
    }
}

function mobile(){
    for(i=1;i<=3;i++){

        if(document.getElementById(`mobile-${i}`).checked == true){
            for( j=0;j<3;j++){
                var content = document.getElementsByClassName("tab-panel")[j];

                content.style.display = "none";

            }
            document.getElementById(`panel-${i}`).style.display = "block";

        }
    }
}


var deadline = document.getElementById('deadline');
// Set the date we're counting down to
var countDownDate = new Date(deadline.dataset.value).getTime();

// Update the count down every 1 second
var x = setInterval(function() {

    // Get today's date and time
    var now = new Date().getTime();

    // Find the distance between now and the count down date
    var distance = countDownDate - now;

    // Time calculations for days, hours, minutes and seconds
    var days = Math.floor(distance / (1000 * 60 * 60 * 24));
    var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    var seconds = Math.floor((distance % (1000 * 60)) / 1000);

    // Output the result in an element with id="demo"
    // document.getElementById("deadline").innerHTML = days + "d " + hours + "h "
    // + minutes + "m " + seconds + "s ";
    document.getElementById("days").innerHTML = days
    document.getElementById("hours").innerHTML = hours
    document.getElementById("minutes").innerHTML = minutes
    document.getElementById("seconds").innerHTML = seconds

    // If the count down is over, write some text
    if (distance < 0) {
        clearInterval(x);
        document.getElementById("deadline").innerHTML = "EXPIRED";
    }
}, 1000);


(function($) {
    "use strict"; // Start of use strict

    // Smooth scrolling using jQuery easing
    $('a.js-scroll-trigger[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        if (target.length) {
            $('html, body').animate({
            scrollTop: (target.offset().top)
            }, 1000, "easeInOutExpo");
            return false;
        }
        }
    });

    // Closes responsive menu when a scroll trigger link is clicked
    $('.js-scroll-trigger').click(function() {
        $('.navbar-collapse').collapse('hide');
    });

    // Activate scrollspy to add active class to navbar items on scroll
    $('body').scrollspy({
        target: '#toc'
    });

})(jQuery); // End of use strict
