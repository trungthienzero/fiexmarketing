<!DOCTYPE html>

<html lang="vi">

<head>
  <meta charset="<?php bloginfo('charset'); ?>">
  <link rel="alternate" href="<?php the_permalink(); ?>" hreflang="vi" />
  <link rel="alternate" href="<?php the_permalink(); ?>" hreflang="x-default" />
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <?php wp_head(); ?>

</head>

<body <?php body_class() ?>>
  <!-- Header -->
  <?php
    if(is_single()){?>
      <header id="site-header" class="minimal-header header-replace single" itemscope itemtype="http://schema.org/WebSite" itemid="#website">
    <?php }else { ?>
      <header id="site-header" class="minimal-header header-replace" itemscope itemtype="http://schema.org/WebSite" itemid="#website">
    <?php }
  ?>
    <meta itemprop="url" content="https://gtvseo.com" />
    <meta itemprop="name" content="GTV SEO">
    <div id="site-header-inner">
      <div id="site-logo">
        <div id="site-logo-inner" itemscope itemtype="http://schema.org/Organization" itemid="#publisher">
          <meta itemprop="url" content="https://gtvseo.com">
          <meta itemprop="name" content="GTV SEO">
          <a href="https://gtvseo.com" class="custom-logo-link" rel="home" itemprop="logo" itemscope itemtype="http://schema.org/ImageObject">
            <meta itemprop="url" content="https://fiexmarketing.com/wp-content/uploads/2020/08/35e96f978d1f7141280e-1024x444-1.png" />
            <img src="https://fiexmarketing.com/wp-content/uploads/2020/08/35e96f978d1f7141280e-1024x444-1.png" class="custom-logo lazyloaded" alt="Fiex Marketing">
          </a>
          <span class="tooltiptext">Fiex Marketing</span>
        </div>
      </div>
      <div id="site-navigation-wrap">
        <nav id="site-navigation" class="navigation main-navigation" role="navigation">
          <?php
          // get_template_part('templates/menu')
          wp_nav_menu(array(
            'theme_location' => 'headerMenuLocation'
          ));
          ?>
          <div class="topnav-cart">
              <ul>
                  <li class="circle">
                      <a href="#search" id="menu-search-btn">
                          <i class="fa fa-search"></i>
                      </a>
                  </li>
              </ul>
          </div>
        </nav>
      </div>

      <div class="oceanwp-mobile-menu-icon mobile-right">
          <a href="#search" class="search-btn">
              <i class="fa fa-search"></i>
          </a>
          <div class="hamburger-menu">
              <div class="mobile-menu" id="open">
                  <div class="mobile-menu-line menu-line-1"></div>
                  <div class="mobile-menu-line menu-line-2"></div>
                  <div class="mobile-menu-line menu-line-3"></div>
              </div>
          </div>
      </div>
    </div>
  </header>
  <!-- End of Header -->


  <!-- Search -->
  <div id="search">
      <button type="button" class="close">×</button>
      <form>
          <input id="search-term" type="search" value="" placeholder="Nhập từ khoá tìm kiếm" required="">
          <button type="submit" class="primary-button"><i class="fa fa-search"></i></button>
      </form>
      <div class="container search-result-container">
          <div id="search-overlay-result"></div>
      </div>
  </div>
  <!--  -->