<!-- Related Post Section -->
<?php if(get_field('related_posts')):?>
    <section class="related-post">
        <div class="related-post-intro">
            <h3 class="title">Bài viết liên quan</h3>
        </div>
        <div class="related-post-content">
            <div class="pure-g">

                <?php $post_objects = get_field('related_posts'); ?>
                    <?php foreach( $post_objects as $post): // variable must be called $post (IMPORTANT) ?>
                    <?php setup_postdata($post); ?>
                        <div class="pure-u-1 pure-u-md-1-3 center">
                            <div class="post-box">
                                <a href="<?php the_permalink(); ?>">
                                    <div class="post-img">
                                        <?php if (has_post_thumbnail()) { ?>
                                            <img src="<?php the_post_thumbnail_url() ?>" alt="<?php the_title(); ?>">
                                        <?php } else { ?>
                                            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/logo.png" alt="">
                                        <?php } ?>
                                    </div>
                                    <div class="post-info-box">
                                        <p class="post-title"><?php the_title(); ?></p>
                                        <ul class="post-info">
                                            <!-- <li><i class="far fa-clock"></i><?php echo get_the_date(); ?></li> -->
                                            <!-- <li><i class="far fa-user-circle"></i><?php the_author(); ?></li> -->

                                            <script>
                                                window.onload = function(){
                                                    const color = ["#0909AD", "#00BC70"];

                                                    const random = Math.floor(Math.random() * color.length);

                                                    var related = document.getElementsByClassName('related-category-name');
                                                    for(i=0; i<related.length; i++){
                                                        related[i].style.backgroundColor = color[random];
                                                    }
                                                };
                                            </script>

                                            <!-- <i class="far fa-folder"></i> -->
                                            <?php $category_detail = get_the_category(get_the_ID());
                                                foreach ($category_detail as $cd) {
                                                    ?>
                                                    <li class="related-category-name"><?php echo $cd->cat_name;?></li>
                                                <?php }
                                            ?>

                                        </ul>
                                    </div>
                                </a>
                                <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly
                                ?>
                            </div>
                        </div>
                    <?php endforeach; ?>
                <?php wp_reset_postdata(); ?>
            </div>
        </div>
    </section>
<?php endif; ?>
<!-- /Related Post Section -->