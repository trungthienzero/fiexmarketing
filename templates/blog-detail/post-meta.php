<?php defined('ABSPATH') or die('This script cannot be accessed directly.');

/**
 * Outputs one single post.
 *
 * (!) Should be called after the current $wp_query is already defined
 *
 * @var $metas array Meta data that should be shown: array('date', 'author', 'categories', 'comments')
 * @var $show_tags boolean Should we show tags?
 *
 * @action Before the template: 'us_before_template:templates/blog/single-post'
 * @action After the template: 'us_after_template:templates/blog/single-post'
 * @filter Template variables: 'us_template_vars:templates/blog/single-post'
 */

// $us_layout = US_Layout::instance();

// Filling and filtering parameters
$default_metas = array('date', 'author', 'categories', 'comments');
$metas = (isset($metas) and is_array($metas)) ? array_intersect($metas, $default_metas) : $default_metas;

$post_format = get_post_format() ? get_post_format() : 'standard';

// Meta => certain html in a proper order
$meta_html = array_fill_keys($metas, '');

// Preparing post metas separately because we might want to order them inside the .w-blog-post-meta in future
$meta_html['date'] = '<time class="blog-post-meta-date date updated';
if (!in_array('date', $metas)) {
	// Hiding from users but not from search engines
	$meta_html['date'] .= ' hidden';
}
$meta_html['date'] .= '"><i class="far fa-clock"></i> ' . get_the_date() . '</time>';

$meta_html['author'] = '<span class="blog-post-meta-author author';
if (!in_array('author', $metas)) {
	$meta_html['author'] .= ' hidden';
}
$meta_html['author'] .= '"itemscope itemtype="http://schema.org/Person" itemid="#person"><i class="far fa-user-circle"></i> ';
if (get_the_author_meta('url')) {
	if(get_the_author() == 'Vincent Do'){
		$meta_html['author'] .= '<a href="https://gtvseo.com/vincent-do/" class="fn" itemprop="url"><span itemprop="name">' . get_the_author() . '</span></a>';
	}
	if(get_the_author() == 'Thu Ho'){
		$meta_html['author'] .= '<a href="https://gtvseo.com/thu-ho/" class="fn" itemprop="url"><span itemprop="name">' . get_the_author() . '</span></a>';
	}
} else {
	$meta_html['author'] .= '<span class="fn" itemprop="name">' . get_the_author() . '</span>';
}
$meta_html['author'] .= '</span>';

if (in_array('categories', $metas)) {
	$meta_html['categories'] = get_the_category_list(', ');
	if (!empty($meta_html['categories'])) {
		$meta_html['categories'] = '<span class="blog-post-meta-category"><i class="far fa-folder"></i> ' . $meta_html['categories'] . '</span>';
	}
}

$meta_html = apply_filters('us_single_post_meta_html', $meta_html, get_the_ID());
?>

<section>
	<div class="l-section-h i-cf">
		<div class="w-blog">
			<div class="w-blog-post-body">
				<div class="blog-header">
					<h1 class="blog-post-title entry-title"><?php the_title() ?></h1>
					<div class="blog-post-meta<?php echo empty($metas) ? ' hidden' : '' ?>">
						<?php echo implode('', $meta_html) ?>
						<?php
							if(get_field('read_time')){ ?>
								<span class="read-time"><?php the_field('read_time') ?> phút đọc</span>
							<?php }
						?>
						<?php
							$actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
						?>
						<ul class="social-share" id="social-share">
							<li>
								<a class="btn-social-icon btn-facebook" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $actual_link; ?>" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/facebook.png" alt="" class="facebook"></a>
							</li>
							<!-- <li>
								<a class="btn btn-social-icon btn-google" href="https://plus.google.com/share?url=<?php echo $actual_link; ?>" target="_blank"><i class="fa fa-google"></i></a>
							</li> -->
							<li>
								<a class="btn-social-icon btn-twitter" href="https://twitter.com/home?status=<?php echo $actual_link; ?>" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/twitter.png" alt="" class="twitter"></a>
							</li>
							<li>
								<a class="btn-social-icon btn-linkedin" href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo $actual_link; ?>&title=<?php echo get_the_title(get_the_ID()); ?>&source=example.com" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/link.png" alt="" class="linkedin"></a>
							</li>
							<!-- <li>
								<a class="btn btn-social-icon btn-pinterest" href="https://www.pinterest.com/pin/create/link/?url=<?php echo $actual_link; ?>&media=<?php echo the_post_thumbnail_url('large'); ?>&description=<?php echo get_the_title(get_the_ID()); ?>" target="_blank"><span class="fa fa-pinterest"></span></a>
							</li> -->
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
