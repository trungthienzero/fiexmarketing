<!-- Breadcrumb -->
<section class="breadcrumb" itemprop="BreadcrumbList" itemscope itemtype="https://schema.org/BreadcrumbList" itemid="#breadcrumb">
    <div class="breadcrumb-box">
        <ul>
            <span itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                <meta itemprop="position" content="1">
                    <a itemprop="item" href="https://gtvseo.com/">
                        <span itemprop="name">Trang chủ</span>
                    </a> »
            </span>
            <span itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                <meta itemprop="position" content="2">
                <?php
                    $term_list = wp_get_post_terms($post->ID, 'category', ['fields' => 'all']);

                    if(!is_singular('post')){
                        $post_type =  get_post_type($post->ID);
                        $obj = get_post_type_object( $post_type);

                        $permalink = get_post_type_archive_link( $post_type );
                        $post_type_name = $obj->labels->name; ?>

                        <a itemprop="item" href="<?php echo $permalink ?>">
                            <span itemprop="name"><?php echo $post_type_name ?></span>
                        </a> »

                    <?php } else{
                        foreach($term_list as $term) {
                            if( get_post_meta($post->ID, '_yoast_wpseo_primary_category',true) == $term->term_id ) {?>

                                <a itemprop="item" href="<?php echo get_term_link($term) ?>">
                                    <span itemprop="name"><?php echo $term->name ?></span>
                                </a> »

                            <?php }
                        }
                    }
                ?>
            </span>
            <span itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                <meta itemprop="position" content="3">
                <span itemprop="item" content="<?php the_permalink() ?>">
                    <span class="breadcrumb_last" aria-current="page" itemprop="name"><?php the_title() ?></span>
                </span>
            </span>
        </ul>
    </div>
</section>
<!-- End of Breadcrumb -->