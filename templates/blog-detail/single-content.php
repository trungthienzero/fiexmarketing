<?php defined('ABSPATH') or die('This script cannot be accessed directly.');

/**
 * Outputs one single post.
 *
 * (!) Should be called after the current $wp_query is already defined
 *
 * @var $metas array Meta data that should be shown: array('date', 'author', 'categories', 'comments')
 * @var $show_tags boolean Should we show tags?
 *
 * @action Before the template: 'us_before_template:templates/blog/single-post'
 * @action After the template: 'us_after_template:templates/blog/single-post'
 * @filter Template variables: 'us_template_vars:templates/blog/single-post'
 */

// $us_layout = US_Layout::instance();

// Filling and filtering parameters
if (!isset($show_tags)) {
	$show_tags = TRUE;
}

// Note: it should be filtered by 'the_content' before processing to output
$the_content = get_the_content();


if (!post_password_required()) {
	$the_content = apply_filters('the_content', $the_content);
}


if ($show_tags) {
	$the_tags = get_the_tag_list('', ', ', '');
}
?>

<?php echo $the_content ?>

<?php if ($show_tags and !empty($the_tags)) : ?>
	<section class="l-section for_tags">
		<div class="l-section-h i-cf">
			<div class="g-tags">
				<span class="g-tags-title"><?php _e('Tags', 'us') ?>:</span>
				<?php echo $the_tags ?>
			</div>
		</div>
	</section>
<?php endif; ?>

<?php
$link = strval(get_permalink());
?>
