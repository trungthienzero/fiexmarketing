<div class="author-box">
    <div class="author-wrap">
        <div class="pure-g">
            <div class="pure-u-1 pure-u-md-1-4">
                <div class="author-img">
                    <span itemscope itemprop="image" class="img-wrap" alt="Photo of <?php the_author_meta('display_name'); ?>">
                        <?php

                        $author_id=$post->post_author;

                        if(get_the_author_meta( 'image_link', $author_id )){ ?>
                            <img src="<?php echo esc_attr( get_the_author_meta( 'image_link', $author_id ) ); ?>" alt="">
                        <?php }
                        else{
                            if (function_exists('get_avatar')) {
                                echo get_avatar(get_the_author_meta('email'), '100');
                            }
                        }?>

                    </span>
                </div>
            </div>
            <div class="pure-u-1 pure-u-md-3-4">
                <div class="author-info" itemscope itemtype="http://schema.org/Person" itemid="#author">
                    <p class="author-name">
                        <span itemprop="name">
                            <?php the_author_meta('display_name'); ?>
                        </span>
                    </p>

                    <p><?php the_author_meta('description') ?></p>

                    <!-- <a href="<?php if ( get_the_author_meta('display_name') == "Vincent Do" ):
                                    echo esc_url("https://gtvseo.com/vincent-do/");
                                elseif ( get_the_author_meta('display_name') == "Thu Ho" ):
                                    echo esc_url("https://gtvseo.com/thu-ho/");
                                endif;
                            ?>" itemprop="url">
                        <button>Xem thêm bài viết của tác giả</button>
                    </a> -->

                    <div class="social-share mt2">
                        <?php if(get_the_author_meta('twitter')){ ?>
                            <a href="<?php the_author_meta('twitter'); ?>" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/twitter.png" alt="" class="twitter"></a>
                        <?php } ?>

                        <?php if(get_the_author_meta('facebook')){ ?>
                            <a href="<?php the_author_meta('facebook'); ?>" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/facebook.png" alt="" class="facebook"></a>
                        <?php } ?>

                        <?php if(get_the_author_meta('instagram')){ ?>
                            <a href="<?php the_author_meta('instagram'); ?>" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/ig.png" alt="" class="ig"></a>
                        <?php } ?>

                        <?php if(get_the_author_meta('linkedin')){ ?>
                            <a href="<?php the_author_meta('linkedin'); ?>" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/link.png" alt="" class="linkedin"></a>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>