<?php get_header(); ?>

<!--  -->
<div id="category-menu">
  <div class="category-wrap">
    <div class="pure-g">
      <div class="pure-u-1 pure-u-md-1-2">
        <div class="category">
          <ul>
            <li><a href="https://gtvseo.com/kien-thuc-facebook-marketing/" target="_blank">Facebook Marketing</a></li>
            <li><a href="https://gtvseo.com/seo/">SEO</a></li>
            <li><a href="https://gtvseo.com/inbound-marketing/">Inbound Marketing</a></li>
          </ul>
        </div>
      </div>
      <div class="pure-u-1 pure-u-md-1-2">
        <div class="form-hub">
          <!--[if lte IE 8]>
                        <script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2-legacy.js"></script>
                        <![endif]-->
          <script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2.js"></script>
          <script>
            hbspt.forms.create({
              portalId: "5312083",
              formId: "c850f509-45e3-4c2e-ae4a-08b12a4103e2",
              css: ' ',
              cssClass: 'test',
              locale: 'vi',
              translations: {
                vi: {
                  submitText: "Nhận tài liệu SEO",
                }
              }
            });
          </script>
        </div>
      </div>
    </div>
  </div>
</div>
<!--  -->

<!-- Breadcrumb -->
<section class="breadcrumb">
  <div class="breadcrumb-box">
    <?php
    if (function_exists('yoast_breadcrumb')) {
      yoast_breadcrumb('<ul>', '</ul>');
    }
    ?>
  </div>
</section>
<!-- End of Breadcrumb -->


<!-- Blog Content -->
<section class="blog">
  <div class="blog-box">
    <div class="pure-g container">
      <div class="pure-u-1 pure-u-md-1-4" id="sidebar">
        <div class="sidebar_inner" id="sidebar_inner">
          <p class="title"><strong>Nội dung bài viết</strong></p>
          <ul class="table-of-contents">
          </ul>
        </div>
      </div>
      <div class="pure-u-1 pure-u-md-3-4" id="content">

        <div class="w-blog-list">
          <?php
          while (have_posts()) {
            the_post();


            get_template_part( 'templates/blog-detail/single-content', $template_vars );
          }

          ?>

          <?php get_template_part('templates/blog-detail/author-box') ?>
        </div>

      </div>
    </div>
  </div>
  <div class="banner-qc" id="QC">
    <a href="https://gtvseo.com/dich-vu-seo-ho-chi-minh/?utm_campaign=BannerWebsite&?utm_source=Website&utm_medium=ClickBanner&utm_content=160x600">
      <img src="/wp-content/themes/gtvseo/images/banner-qc.png" alt="quảng cáo gtv">
    </a>
  </div>
</section>
<!-- /Blog Content -->

<?php
get_template_part('templates/blog-detail/related-posts')
?>

<?php get_footer(); ?>