<div id="category-menu">
    <div class="category-wrap">
        <!-- <div class="pure-g">
            <div class="pure-u-1 pure-u-md-2-3">
                <div class="category">
                    <?php
                        wp_nav_menu(array(
                            'theme_location' => 'blogsubmenu'
                        ));
                    ?>
                </div>
                <section class="breadcrumb" itemprop="BreadcrumbList" itemscope itemtype="https://schema.org/BreadcrumbList" itemid="#breadcrumb">
                    <div class="breadcrumb-box">
                        <ul>
                            <span itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                                <meta itemprop="position" content="1">
                                    <a itemprop="item" href="https://gtvseo.com/">
                                        <span itemprop="name">Trang chủ</span>
                                    </a> »
                            </span>
                            <span itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                                <meta itemprop="position" content="2">
                                <?php
                                    $term_list = wp_get_post_terms($post->ID, 'category', ['fields' => 'all']);

                                    if(!is_singular('post')){
                                        $post_type =  get_post_type($post->ID);
                                        $obj = get_post_type_object( $post_type);

                                        $permalink = get_post_type_archive_link( $post_type );
                                        $post_type_name = $obj->labels->name; ?>

                                        <a itemprop="item" href="<?php echo $permalink ?>">
                                            <span itemprop="name"><?php echo $post_type_name ?></span>
                                        </a> »

                                    <?php } else{
                                        foreach($term_list as $term) {
                                            if( get_post_meta($post->ID, '_yoast_wpseo_primary_category',true) == $term->term_id ) {?>

                                                <a itemprop="item" href="<?php echo get_term_link($term) ?>">
                                                    <span itemprop="name"><?php echo $term->name ?></span>
                                                </a> »

                                            <?php }
                                        }
                                    }
                                ?>
                            </span>
                            <span itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                                <meta itemprop="position" content="3">
                                <span itemprop="item" content="<?php the_permalink() ?>">
                                    <span class="breadcrumb_last" aria-current="page" itemprop="name"><?php the_title() ?></span>
                                </span>
                            </span>
                        </ul>
                    </div>
                </section>
            </div>
            <div class="pure-u-1 pure-u-md-1-3">
                <div class="blog-sub-menu">
                    <ul>
                        <li>
                            <a href="#search" id="menu-search-btn">
                                <i class="fa fa-search"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#hubspot" id="hubspot-toggle">Theo dõi</a>
                            <div id="hubspot-wrap">
                                <div class="form-hub">
                                    [if lte IE 8]>
                                            <script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2-legacy.js"></script>
                                            <![endif]
                                    <script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2.js"></script>
                                    <script>
                                        hbspt.forms.create({
                                            portalId: "5312083",
                                            formId: "c850f509-45e3-4c2e-ae4a-08b12a4103e2",
                                            locale: 'vi',
                                            css: '',
                                            cssClass:'custom-form',
                                            translations: {
                                                vi: {
                                                    submitText: "Nhận tài liệu",
                                                }
                                            }
                                        });
                                    </script>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div> -->

        <div id="hubspot-wrap">
            <div class="form-hub">
                <!-- [if lte IE 8]>
                        <script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2-legacy.js"></script>
                        <![endif] -->
                <script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2.js"></script>
                <script>
                    hbspt.forms.create({
                        portalId: "5312083",
                        formId: "c850f509-45e3-4c2e-ae4a-08b12a4103e2",
                        locale: 'vi',
                        css: '',
                        cssClass:'test',
                        // cssClass:'custom-form',
                        translations: {
                            vi: {
                                submitText: "Nhận tài liệu",
                            }
                        }
                    });
                </script>
            </div>
        </div>
    </div>
    <div class="progress-container">
        <div class="progress-bar" id="myBar"></div>
    </div>
</div>