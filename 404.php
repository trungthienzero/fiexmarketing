<?php
get_header();
?>
    <div class="error-page">
        <!--  -->
        <section class="intro">
            <div class="title-wrap">
                <h1 class="title">Tèn ten!<br>Lỗi 404 - Trang này không tồn tại</h1>
                <p>Vì sao bạn lại thấy trang 404 này? Hãy để các thành viên GTV Team giải thích cho bạn về lỗi này và giúp bạn điều hướng khỏi trang.</p>
                <p>Với GTV, kiến thức là miễn phí!</p>
            </div>
        </section>
        <!--  -->

        <!--  -->
        <section class="suggestion">
            <div class="suggestion-wrap">
                <div class="pure-g">
                    <div class="pure-u-md-1-4">
                        <div class="suggest-box">
                            <div class="img-box-wrap">
                                <div class="img-box" onclick="openContent()">
                                    <img src="/wp-content/themes/gtvseo/images/content-404.png" alt="" class="nothover">
                                    <img src="/wp-content/themes/gtvseo/images/content-hover.png" alt="" class="onhover">
                                </div>
                            </div>
                            <div class="btn">
                                <button onclick="openContent()">Team Content</button>
                            </div>
                        </div>
                    </div>
                    <div class="pure-u-md-1-4">
                        <div class="suggest-box">
                            <div class="img-box-wrap">
                                <div class="img-box" onclick="openAdmin()">
                                    <img src="/wp-content/themes/gtvseo/images/admin-404.png" alt="" class="nothover">
                                    <img src="/wp-content/themes/gtvseo/images/admin-hover.png" alt="" class="onhover">
                                </div>
                            </div>
                            <div class="btn">
                                <button onclick="openAdmin()">Team Sales</button>
                            </div>
                        </div>
                    </div>
                    <div class="pure-u-md-1-4">
                        <div class="suggest-box">
                            <div class="img-box-wrap">
                                <div class="img-box" onclick="openSeo()">
                                    <img src="/wp-content/themes/gtvseo/images/SEO-404.png" alt="" class="nothover">
                                    <img src="/wp-content/themes/gtvseo/images/SEO-hover.png" alt="" class="onhover">
                                </div>
                            </div>
                            <div class="btn">
                                <button onclick="openSeo()">Team SEO</button>
                            </div>
                        </div>
                    </div>
                    <div class="pure-u-md-1-4">
                        <div class="suggest-box">
                            <div class="img-box-wrap">
                                <div class="img-box" onclick="openIt()">
                                    <img src="/wp-content/themes/gtvseo/images/IT-404.png" alt="" class="nothover">
                                    <img src="/wp-content/themes/gtvseo/images/IT-hover.png" alt="" class="onhover">
                                </div>
                            </div>
                            <div class="btn">
                                <button onclick="openIt()">Team IT</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--  -->

        <!-- Suggestion Popup -->
        <div class="popup" id="content-popup">
            <div class="popup-wrap">
                <div class="popup-box-wrap">
                    <div class="popup-box slideUp">
                        <div class="pure-g">
                            <div class="pure-u-md-1-3">
                                <div class="img-box-wrap">
                                    <div class="img-box">
                                        <img src="/wp-content/themes/gtvseo/images/content-hover.png" alt="">
                                    </div>
                                </div>
                            </div>
                            <div class="pure-u-md-2-3">
                                <div class="suggest-content-wrap">
                                    <div class="suggest-content">
                                        <p>Dường như URL bạn nhập chưa đúng lắm. Nhập sai URL là một lỗi phổ biến dẫn đến trang 404. Là một trong những thành phần "nhớ mặt đặt tên" URL nhưng team Content cũng thường xuyên nhập sai URL vì đánh máy nhanh hoặc nhớ nhầm đường dẫn ^^</p>
                                        <p>Hoặc chế độ viết lại URL (mod_rewrite) đã bị tắt (đây là mô-đun Apache cho phép bạn tùy chỉnh các URL trên website). Và trường hợp xấu nhất thì có lẽ bài viết đã bị xóa rồi (tuy vậy trường hợp này rất hiếm)!</p>

                                        <p><strong>Lời khuyên:</strong></p>
                                        <p>Bạn có thể nhập nội dung/tên bài viết vào thanh tìm kiếm Google kèm cụm "gtv" để tìm ra bài viết chính xác! Nếu bài viết trên Google vẫn không truy cập được, hẳn trang đã bị lỗi rồi. Bạn nhắn lại admin giúp nhận diện lỗi và cải thiện bài viết nhé!</p>
                                        <p>Trong lúc chờ team Content bị sa thải sửa lỗi, bạn có thể tham khảo các bài viết khác mà team Content sắp bị sa thải nghĩ rằng bạn sẽ thích ở đây 👇</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <a id="close" onclick="closePop()">X</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="popup" id="admin-popup">
            <div class="popup-wrap">
                <div class="popup-box-wrap">
                    <div class="popup-box slideUp">
                        <div class="pure-g">
                            <div class="pure-u-md-1-3">
                                <div class="img-box-wrap">
                                    <div class="img-box">
                                        <img src="/wp-content/themes/gtvseo/images/admin-hover.png" alt="">
                                    </div>
                                </div>
                            </div>
                            <div class="pure-u-md-2-3">
                                <div class="suggest-content-wrap">
                                    <div class="suggest-content">
                                        <p>Rất có thể trang đã không được tải đúng. Một vài sự cố đường truyền là thủ phạm gây ra lỗi này. Hoặc trường hợp khác là bạn Sales Admin đã đưa một chiếc URL sai, bởi vì bạn Sale Admin nhớ /đánh máy không đúng đường dẫn, dù chỉ dư 1 ký tự cũng sẽ không truy cập được vào bài viết chính xác, dẫn bạn đến "neverlink" - một trang không tồn tại. Hoặc rất có thể bài viết đã bị đổi URL, hoặc bị xóa.</p>
                                        <p>Trong một vài trường hợp thì lỗi bộ nhớ cũng gây nên trang 404.</p>

                                        <p><strong>Lời khuyên:</strong></p>
                                        <p>Cách đơn giản nhất Account thường áp dụng là F5 lại bài viết một lần, đây là cách xử lý lỗi đường truyền nhanh nhất. Nếu không có hiệu quả, Account sẽ thử xóa bộ nhớ cache của trình duyệt và xóa cookie, phương pháp này cũng thường có hiệu quả, đặc biệt với các trường hợp truy cập web bằng điện thoại di động. Nếu vẫn không thành công, bạn có thể báo lại với Sale Amdin về sự cố này và nhờ hỗ trợ. Đội ngũ Sales GTV luôn sẵn lòng lắng nghe, giúp đỡ và cải thiện! 😉</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <a id="close" onclick="closePop()">X</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="popup" id="seo-popup">
            <div class="popup-wrap">
                <div class="popup-box-wrap">
                    <div class="popup-box slideUp">
                        <div class="pure-g">
                            <div class="pure-u-md-1-3">
                                <div class="img-box-wrap">
                                    <div class="img-box">
                                        <img src="/wp-content/themes/gtvseo/images/SEO-hover.png" alt="">
                                    </div>
                                </div>
                            </div>
                            <div class="pure-u-md-2-3">
                                <div class="suggest-content-wrap">
                                    <div class="suggest-content">
                                        <p>Khả năng cao bài viết này đổi URL nhưng chưa được redirect. Một vài bài viết của GTV sẽ được đổi URL nhằm đảm bảo tính năng SEO và khả năng nhận diện, ghi nhớ. Sau khi đổi link, URL mới của bài viết sẽ được redirect với URL cũ giúp dẫn người đọc dù nhập link trước đó vẫn truy cập được bài viết. Tuy nhiên, đôi khi team SEO sẽ quên mất nhiệm vụ redirect hoặc chưa được báo lại về việc đổi URL này.</p>
                                        <p>Một trường hợp khác là có thể trang đã bị xóa nhưng đường dẫn vẫn được người đọc yêu thích lưu lại, hoặc bị giới hạn quyền truy cập ngoài ý muốn. Nếu bạn gặp phải loại sự cố này, bạn sẽ nhận một thông báo như "Bạn không được phép truy cập ..."</p>

                                        <p><strong>Lời khuyên:</strong></p>
                                        <p>Giúp team SEO sửa chữa lỗi này bằng cách báo cáo với admin thường trực website. Cùng lúc đó, bạn có thể nhập tên/nội dung bài viết để tìm bài gốc tại đây. Hoặc tìm kiếm theo các đề tài lớn dưới đây nhé! Một cách nhanh hơn: chat trực tiếp với team Admin website thường trực để được giúp đỡ ngay và luôn!</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <a id="close" onclick="closePop()">X</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="popup" id="it-popup">
            <div class="popup-wrap">
                <div class="popup-box-wrap">
                    <div class="popup-box slideUp">
                        <div class="pure-g">
                            <div class="pure-u-md-1-3">
                                <div class="img-box-wrap">
                                    <div class="img-box">
                                        <img src="/wp-content/themes/gtvseo/images/IT-hover.png" alt="">
                                    </div>
                                </div>
                            </div>
                            <div class="pure-u-md-2-3">
                                <div class="suggest-content-wrap">
                                    <div class="suggest-content">
                                        <p>Rất có thể máy chủ đã bị giới hạn bộ nhớ (lỗi memory_limit) hoặc sự cố với tệp .htaccess, đây là một tệp cấu hình được sử dụng bởi máy chủ web Apache trong thư mục gốc, trong số những thứ khác, có thể kiểm soát các chuyển hướng, bảo vệ các thư mục của bạn và viết lại URL.</p>
                                        <p>Trong nhiều trường hợp, lỗi 404 có thể được giải quyết bằng cách tạo lại tệp này.</p>
                                        <p>Một lỗi khác nữa là cấu hình chứng chỉ SSL không chính xác. Nếu bạn cài đặt chứng chỉ SSL và trang web của bạn có sự bất thường này, điều đó có nghĩa là chứng chỉ chưa được cài đặt chính xác.</p>
                                        <p>Xác minh cấu hình của chứng chỉ SSL với máy chủ web của bạn và đảm bảo rằng phiên bản "SSL" của URL được kết nối với (hoặc chuyển hướng đến) URL chính xác trong tệp .htaccess.</p>

                                        <p><strong>Lời khuyên:</strong></p>
                                        <p>Bạn giúp team IT báo link sai nhé! Team IT sẽ sửa lỗi nhanh nhất có thể! ✔️</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <a id="close" onclick="closePop()">X</a>
                    </div>
                </div>
            </div>
        </div>
        <!--  -->

        <div class="last-text">
            <p class="center">Đã xem hết? Okay! Trở về <a href="https://gtvseo.com">Trang chủ.</a> 💖</p>
        </div>
    </div>

    <script>
        function openContent(){
            document.getElementById('content-popup').style.display = "block";
        }
        function openSeo(){
            document.getElementById('seo-popup').style.display = "block";
        }
        function openIt(){
            document.getElementById('it-popup').style.display = "block";
        }
        function openAdmin(){
            document.getElementById('admin-popup').style.display = "block";
        }
        function closePop(){
            document.getElementById('content-popup').style.display = "none";
            document.getElementById('seo-popup').style.display = "none";
            document.getElementById('it-popup').style.display = "none";
            document.getElementById('admin-popup').style.display = "none";
        }

        // window.addEventListener("click", function(event) {
        //     document.getElementById('content-popup').style.display = "none";
        //     document.getElementById('seo-popup').style.display = "none";
        //     document.getElementById('it-popup').style.display = "none";
        //     document.getElementById('admin-popup').style.display = "none";
        // });

        $('.popup').click(function(){
            document.getElementById('content-popup').style.display = "none";
            document.getElementById('seo-popup').style.display = "none";
            document.getElementById('it-popup').style.display = "none";
            document.getElementById('admin-popup').style.display = "none";
        });
    </script>
<?php
get_footer();
?>